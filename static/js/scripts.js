$(document).ready(function () {

    $(".nav-div").click(function () {
        window.location = $(this).find(".site-link").attr("href");
    });
    // Initialize comment create
    $("#create-comment").click(function () {
        let post_id = $("#post-title").data("post-id");
        let text = $("#new_text").val();
        if (text !== "") {
            $.post({
                url: "/comments/new/" + post_id,
                data: {
                    csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                    text: text,
                    post: post_id
                }
            }).done((res) => {
                $("div.list-group").prepend(res);
                initialize_edit_comment();
            });
        }
    });

    initialize_edit_comment();
    initialize_delete_comment();

});

function initialize_edit_comment() {
    $(".edit-comment").each(function () {
        $(this).click(function () {
            let id = $(this).parent().attr("id");

            $.get({
                url: "/comments/" + id + "/edit"
            }).done((res) => {
                $(this).prev(".mb-1").replaceWith(res);
                $(this).remove();
                initialize_save_button(id);
            })
        })
    });
}

function initialize_delete_comment() {
        // Initialize comment delete button
    $(".delete-comment").each(function () {
        $(this).click(function () {
            let id = $(this).parent().attr("id");

            $("#confirm-comment-delete-modal").modal("show");
            initialize_confirm_delete(id);
        });
    });
}

function initialize_save_button(id) {
    $("#save-comment").click(function () {
        $.post({
            url: "/comments/" + id + "/edit",
            data: {
                csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                text: $("#id_text").val()
            }
        }).done((res) => {
            $(this).parent().replaceWith(res);
            initialize_edit_comment();
            initialize_delete_comment();
        })
    });
}

function initialize_confirm_delete(id) {
    $("#delete-confirm").click(function () {
        $.post({
            url: "/comments/" + id + "/delete",
            data: {
                csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
            }
        }).done(() => {
            let selector = "div#" + id;
            $("#confirm-comment-delete-modal").modal("hide");
            // Remove comment
            $(selector).remove();
        })
    });
}

// function initialize_search() {
//     let search_bar = $("#search-bar");
//
//     search_bar.keyup(delayAction(200, function () {
//         let text = search_bar.val();
//
//         // Remove results if user clears input box
//         if (!text) {
//             $('.search-results-container').remove();
//             $("#search-box").removeClass('search-box-active');
//         } else {
//             $.post({
//                 data: {text: text},
//                 url: "/search/autocomplete",
//             }).done(response => autocomplete_results(response));
//         }
//     }));
// }

// function autocomplete_results(response) {
//     // Remove the results div before adding more results
//     $(".search-results-container").remove();
//     $("#search-box").removeClass('search-box-active');
//
//     if (response.length > 0) {
//         if (!$('.search-results-container').length > 0) {
//             $(".search-row").append(
//                 "<div class='col col-8 search-results-container'></div>"
//             );
//             $("#search-box").addClass('search-box-active');
//         }
//
//         response.forEach(row => {
//             let title = row['title'];
//             let text = row['text'];
//             let date = row['date'];
//             let href = row['href'];
//
//             $(".search-results-container").append(
//                 "<div class=\"list-group-item flex-column align-items-start search-result\">\n" +
//                 "    <div class=\"d-flex w-100 justify-content-between\">\n" +
//                 "        <h4>" + title + "</h4>\n" +
//                 "        <small>" + date + "</small>\n" +
//                 "    </div>\n" +
//                 "    <p class=\"mb-1\">" + text + "</p>\n" +
//                 "    <a class=\"site-link\" href=\"" + href + "\"></a>\n" +
//                 "</div>"
//             );
//         });
//     }
// }

// Delay an function a set number of milliseconds, and rebind if function call is repeated
function delayAction(ms, fn) {
    let timer;
    return function (...args) {
        clearTimeout(timer);
        timer = setTimeout(fn.bind(this, ...args), ms);
    }
}