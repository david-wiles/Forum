from django.test import TestCase
from django.urls import reverse

from apps.site.models import ForumUser
from apps.forum_app.models import Topic, Post, Comment


def set_up_forum_tests():
    test_user = ForumUser.objects.create_user(username="test_user", password="8YffHMmPNuuRW4Qy")
    test_moderator = ForumUser.objects.create_user(username="moderator", password="g9sYUGuKVzr5UAMP")

    test_user.save()
    test_moderator.save()

    # Maybe don't do this ...
    #
    # for topic in range(10):
    #     test_topic = Topic.objects.create(title="testTopic" + str(topic), short_description="test topic")
    #     test_topic.save()
    #
    # for topic in Topic.objects.all():
    #     for post in range(10):
    #         post = Post.objects.create(title="testPost" + str(post), text="test post", topic=topic, user=test_user)
    #         post.save()
    #
    # for post in Post.objects.all():
    #     for comment in range(10):
    #         comment = Comment.objects.create(text="testComment" + str(comment), post=post, user=test_user)
    #         comment.save()

    test_topic = Topic.objects.create(title="TestTopic1", short_description="abc123")
    test_post = Post.objects.create(title="TestPost1", text="qwertyqwertyqwertyqwertyqwerty", topic=test_topic, user=test_user)
    test_comment1 = Comment.objects.create(text="comment1", post=test_post, user=test_user)
    test_comment2 = Comment.objects.create(text="comment2", post=test_post, user=test_user)
    test_topic.save()
    test_post.save()
    test_comment1.save()
    test_comment2.save()


def set_up_file_test():
    test_user = ForumUser.objects.create_user(username="test_user", password="8YffHMmPNuuRW4Qy")
    test_moderator = ForumUser.objects.create_user(username="moderator", password="g9sYUGuKVzr5UAMP")

    test_user.save()
    test_moderator.save()


class TopicViewsTest(TestCase):
    @classmethod
    def setUp(cls):
        set_up_forum_tests()

    def test_topic_list(self):
        response = self.client.get(reverse("topic-list"))
        self.assertEqual(response.status_code, 200)


class PostViewsTests(TestCase):
    @classmethod
    def setUp(cls):
        set_up_forum_tests()

    def test_post_list_recent(self):
        response = self.client.get(reverse("posts-list-recent"))
        self.assertEqual(response.status_code, 200)

    def test_post_create(self):
        response = self.client.get(reverse("post-create"))
        self.assertEqual(response.status_code, 200)


class CommentViewsTest(TestCase):
    @classmethod
    def setUp(cls):
        set_up_forum_tests()


class FileViewsTest(TestCase):
    @classmethod
    def setUp(cls):
        set_up_file_test()

    # def test_cloud_index(self):
    #     response = self.client.get(reverse("cloud-index"))
    #     self.assertEqual(response.status_code, 200)


class UserViewsTest(TestCase):
    @classmethod
    def setUp(cls):
        test_user = ForumUser.objects.create_user(username="test_user", password="8YffHMmPNuuRW4Qy")
        test_moderator = ForumUser.objects.create_user(username="moderator", password="g9sYUGuKVzr5UAMP")

        test_user.save()
        test_moderator.save()


    def test_register(self):
        response = self.client.get(reverse("register"))
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        response = self.client.get(reverse("login"))
        self.assertEqual(response.status_code, 200)
