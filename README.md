# Basic Forum

This project showcases a website with basic forum and file sharing capabilities using django.
Follow the steps below to get a development environment up and running...

I developed and tested on a machine running Ubuntu Server 18.04, but any machine with Docker should be able to run it.
If you want to run the website with Ubuntu Server, click [here](https://ubuntu.com/download/server) to download an iso
and use the file to create a virtual machine using VirtualBox. Be sure to forward 0.0.0.0:8000 on the guest
machine to 127.0.0.1:8000 on the host using using NAT.  

1. Install Docker

    If you don't have it already, Docker can be installed by visiting 
    [Docker's website](https://docs.docker.com/install/linux/docker-ce/ubuntu/) 
    and following the instuctions listed to install a secure build.
    
2. Docker-Compose

    Docker-compose builds, connects, and manages multiple docker containers with a single command, 
    making it much easier to set up an environment.  You can install docker-compose 
    with ```sudo apt install docker-compose```
    
3. Pull Images

    The docker-compose orchestration uses the postgres and redis images from Docker Hub. You can pull them
    with ```sudo docker pull postgres``` and ```sudo docker pull redis```. If you choose to skip this step,
    it won't make a difference because docker-compose will do it for you, but might as well make sure that
    you have it, right?
    
4. Build containers

    This step can actually be skipped too. Build the docker-compose image specified by using 
    ```sudo docker-compose build```.
    
5. Start composition

    This step can't be skipped. Start and orchestrate the containers with ```sudo docker-compose up```. 
    The dependencies list should prevent the web server from running before the database is up, but because the 
    web server's startup script depends on the database to run a migration, it might take a try or two. If 
    this happens, and the script attempts to run before the database is accepting connections, you can try 
    again by stopping the containers with ```sudo docker-compose down``` and running them again with 
    ```sudo docker-compose up```.
    
6. Manage the containers

    The server you just started is blank... meaning no topics, no posts, no files. In order to really test the 
    website, you may want to add in some dummy data.  First move the containers to a background process with 
    ```ctrl-z``` followed by ```bg```. Then follow these instructions to manage the containers...
    
    * View container's ID's and names: ```sudo docker ps```
    * Create a superuser: ```sudo docker exec -it web_server python manage.py createsuperuser```
    * Run any django command: ```sudo docker exec -it web_server python manage.py [cmd]```
    * Enter redis cli: ```sudo docker exec -it database redis-cli```
    * Enter postgres cli: ```sudo docker exec -it cache_server psql postgres postgres```
    
The configuration file (.env) in this repository is set up to run with the default configuration created by 
the docker-compose file, so you should be all set!

As a side note, this compose file currently breaks many security rules, and shouldn't be used for anything other than
seeing how the project works... 
