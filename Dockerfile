FROM python:3.6-slim-buster

COPY requirements.txt /requirements.txt
RUN pip install -r requirements.txt
COPY . /
COPY start.sh /start.sh
EXPOSE 8000
