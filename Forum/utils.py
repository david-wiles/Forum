from django.core.paginator import PageNotAnInteger, EmptyPage


def paginator_catch_exceptions(page, paginator):
    # Ensures that the paginator used is always valid
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        items = paginator.page(1)
    except EmptyPage:
        items = paginator.page(paginator.num_pages)

    return items
