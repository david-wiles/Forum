import uuid
from django.db import models
from django.contrib.auth.models import AbstractUser, Group
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.text import slugify


def get_user_file_dir(instance, filename):
    # Get the path to the user's directory on the file system
    return f"{instance.user.id}/{filename}"


class ForumUser(AbstractUser):
    """
    Additional fields added to abstract user:
    Bio     User biography
    Image   User profile picture. Must be uploaded first
    Id      Use uuid instead of incremental integer
    Slug    Path to user page based on username instead of id
    """
    bio = models.TextField(null=True, blank=True)
    image = models.ForeignKey("file_cloud.File", on_delete=models.DO_NOTHING, null=True, blank=True)

    id = models.UUIDField(default=uuid.uuid1, primary_key=True, editable=False)
    slug = models.SlugField(unique=True, editable=False)

    def save(self, *args, **kwargs):
        # Create slug on save if it is empty
        if self.slug == '':
            self.slug = slugify(self.username)
        super(ForumUser, self).save(*args, **kwargs)

    def is_logged_in_user(self, user):
        # Checks if this user is the same as the logged in user
        return self == user

    def __str__(self):
        return self.username


@receiver(post_save, sender=ForumUser)
def set_default_group(sender, instance, **kwargs):
    # Sets the user's group to basic user on creation
    instance.groups.add(Group.objects.get(name="basic_user"))


def is_moderator(user):
    # Checks if a user is a member of the moderator group
    return user.groups.filter(name="moderator").exists()
