from django.apps import AppConfig


class FileCloudConfig(AppConfig):
    name = 'apps.file_cloud'
