from django import forms


class FileForm(forms.Form):
    """
    Only user-editable fields in file are upload and description.
    All other fields are either pre-populated or generated based on upload
    """
    upload = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))
    description = forms.CharField(max_length=2048)


class DirectoryCreate(forms.Form):
    """
    Only user-editable field is directory name. All other fields
    are pre-populated or auto-generated.
    """
    name = forms.CharField(max_length=255)
