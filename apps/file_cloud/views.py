from django.contrib import messages
from django.contrib.auth.decorators import permission_required, login_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.views import generic
from apps.file_cloud.forms import FileForm, DirectoryCreate
from apps.file_cloud.models import File, Directory

@login_required
@permission_required("file_cloud.view_file", login_url="/login")
def cloud_index(request):
    try:
        root = Directory.objects.get(name="root", user=request.user)
        return render(request, "cloud/directory/detail.html", context={'directory': root})
    except ObjectDoesNotExist:
        # Create a root directory for first time visitors
        root = Directory(
            name="root",
            user=request.user
        )
        root.save()
        return render(request, "cloud/directory/detail.html", context={'directory': root})


@permission_required("file_cloud.view_file", login_url="/login")
def file_view(request, slug):
    try:
        file = File.objects.get(slug=slug)
        if file.is_owner(request.user) or file.is_public:
            # The following line should be changed if the default file server is not used
            return redirect(f"/files/{file.user_id}/{file.filename()}")
        else:
            messages.info(request, "File not found or not available.", "alert-info")
            return redirect("/")
    except ObjectDoesNotExist:
        messages.info(request, "File not found or not available.", "alert-info")
        return redirect("/")


@permission_required("file_cloud.add_file", login_url="/login")
def file_upload(request, slug):
    directory = Directory.objects.get(slug=slug, user=request.user)
    if request.method == "POST":
        form = FileForm(request.POST, request.FILES)
        files = request.FILES.getlist("upload")
        if form.is_valid():
            for f in files:
                file = File(
                    upload=f,
                    description=form.cleaned_data.get('description'),
                    directory=directory,
                    user=request.user
                )
                file.save()
            messages.success(request, "Files successfully uploaded!", "alert-success")
            return redirect(directory.get_absolute_url())
        else:
            messages.warning(request, "Something wasn't right. Try uploading again.", "alert-warning")
            return reverse("file-upload")

    form = FileForm()
    return render(request, "cloud/files/upload.html", context={'form': form})


@permission_required("file_cloud.change_file", login_url="/login")
def file_visibility(request, slug):
    if request.method == "POST":
        try:
            file = File.objects.get(slug=slug)
            if file.is_owner(request.user):
                file.is_public = not file.is_public
                file.save()
                messages.success(request, "File visibility updated!", "alert-success")
                return redirect(file.get_absolute_url())
            else:
                messages.error(request, "File not found.", "alert-danger")
                return redirect("cloud-index")
        except ObjectDoesNotExist:
            messages.error(request, "File not found!", "alert-danger")
            return reverse("cloud-index")


@permission_required("file_cloud.view_directory", login_url="/login")
def directory_view(request, slug):
    try:
        directory = Directory.objects.get(slug=slug, user=request.user)
        if directory.is_owner(request.user):
            return render(request, "cloud/directory/detail.html", context={'directory': directory})
    except ObjectDoesNotExist:
        messages.error(request, "Folder does not exist!", "alert-danger")
        return redirect('/cloud')

    return redirect('login')


@permission_required("file_cloud.add_directory", login_url="/login")
def directory_create(request, slug):
    parent = Directory.objects.get(slug=slug, user=request.user)

    if request.method == "POST":
        form = DirectoryCreate(request.POST)
        if form.is_valid():
            directory = Directory(
                name=form.cleaned_data.get('name'),
                user=request.user,
                parent_dir=parent
            )
            directory.save()
            return redirect(directory.get_absolute_url())

    form = DirectoryCreate()
    return render(request, "cloud/directory/create.html", context={'form': form})


class FileDetailView(PermissionRequiredMixin, generic.DetailView):
    permission_required = "file_cloud.view_file"
    login_url = "/login"
    model = File
    template_name = "cloud/files/detail.html"


class FileEditView(PermissionRequiredMixin, generic.UpdateView):
    permission_required = "file_cloud.change_file"
    login_url = "/login"
    model = File
    fields = ["description", "directory"]
    template_name = "cloud/files/edit.html"

    def get_form(self, form_class=None):
        form = super(FileEditView, self).get_form(form_class)
        form.fields["directory"].queryset = Directory.objects.filter(user=self.request.user)
        return form


class FileDeleteView(PermissionRequiredMixin, generic.DeleteView):
    permission_required = "file_cloud.delete_file"
    login_url = "/login"
    model = File
    template_name = "cloud/files/confirm_delete.html"
    success_url = reverse_lazy("cloud-index")


class DirectoryDeleteView(PermissionRequiredMixin, generic.DeleteView):
    permission_required = "file_cloud.delete_directory"
    login_url = "/login"
    model = Directory
    template_name = "cloud/directory/confirm_delete.html"
    success_url = reverse_lazy("cloud-index")
