from django.contrib import admin
from .models import Post, Topic, Comment

admin.site.register(Post)
admin.site.register(Topic)
admin.site.register(Comment)

# Register objects to make select options human-readable

class PostAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug", ("title",)}

class TopicAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}

