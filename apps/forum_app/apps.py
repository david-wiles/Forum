from django.apps import AppConfig


class ForumAppConfig(AppConfig):
    name = 'apps.forum_app'
