import os
import uuid
import datetime

from django.contrib.postgres.indexes import GinIndex
from django.contrib.postgres.search import SearchVector, SearchVectorField
from django.dispatch import receiver
from django.utils.text import slugify
from django.db import models
from django.urls import reverse
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from apps.site.models import ForumUser


class Topic(models.Model):
    """
    Topic model.  Allows users to change title, image, and short description, and automatically creates tsvector tokens,
    uuid, slug based on title, and date created. Is referenced by post.
    """
    title = models.CharField(max_length=255, unique=True)
    image = models.ImageField(upload_to="img/", null=True, blank=True)
    short_description = models.CharField(max_length=255)

    tokens = SearchVectorField(null=True, blank=True, editable=False)
    id = models.UUIDField(default=uuid.uuid1, primary_key=True, editable=False)
    slug = models.SlugField(max_length=255, unique=True, editable=False)
    date_created = models.DateField(auto_now_add=True, editable=False)

    class Meta:
        # Create an index based on search tokens to speed up full text search
        indexes = [GinIndex(fields=['tokens'])]

    def get_absolute_url(self):
        # Return url as "/topics/<slug:slug>"
        return reverse('topic-detail', args=[str(self.slug)])

    def save(self, *args, **kwargs):
        # If the slug is empty, then a slug should be created based on the topic title
        # This ensures that the slug is only changed on the first insert
        if self.slug == '':
            self.slug = slugify(self.title)
        super(Topic, self).save(*args, **kwargs)

    def __str__(self):
        return self.title


@receiver(models.signals.post_delete, sender=Topic)
def auto_delete_file(sender, instance, **kwargs):
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)


class Post(models.Model):
    """
    Post model. Allows users to edit title, text, topic, image, and link, and auto creates tsvector tokens, uuid, slug,
    referenced user, number of visits, date modified, and date created. References topic, user, and file, and is
    referenced by comment
    """
    title = models.CharField(max_length=255)
    text = models.TextField()
    topic = models.ForeignKey('Topic', on_delete=models.CASCADE)
    image = models.ForeignKey("file_cloud.File", on_delete=models.DO_NOTHING, null=True, blank=True)
    link = models.CharField(max_length=1024, null=True, blank=True)

    tokens = SearchVectorField(null=True, blank=True, editable=False)
    id = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
    slug = models.SlugField(max_length=512, unique=True, editable=False)
    user = models.ForeignKey("site.ForumUser", on_delete=models.DO_NOTHING, editable=False)
    num_visits = models.IntegerField(null=False, default=0)
    date_modified = models.DateTimeField(auto_now=True)
    date_created = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        # Create index based on search tokens, and order by date created
        indexes = [GinIndex(fields=['tokens'])]
        ordering = ['date_created']

    def is_owner(self, user: ForumUser):
        # Checks if a user is the user referenced by the post
        return user == self.user

    def get_summary_text(self) -> str:
        # Returns the first 255 characters of text
        return self.text[:255]

    def get_short_date(self) -> str:
        # Formats date created to Month, Day
        return self.date_created.strftime("%b %d")

    def get_time_modified(self):
        # Returns date modified to Month, Day, Hour: Minute: Seconds
        return self.date_modified.strftime("%b %d, %H:%M:%S")

    def get_absolute_url(self):
        # Returns a url like "/posts/<slug:topic-slug>/<slug:post-slug>"
        return reverse('post-view', args=[str(self.topic.slug), str(self.slug)])

    def save(self, *args, **kwargs):
        # Creates a slug based on title and date created if one doesn't exist
        if self.slug == '':
            self.slug = slugify(self.title + str(datetime.datetime.now()))
        super(Post, self).save(*args, **kwargs)

    def __str__(self) -> str:
        return self.title


class Comment(models.Model):
    """
    Model for comment.  Only user editable field is text. Uuid, post, user, date modified, and date created are
    automatically generated. References a post and a user
    """
    text = models.TextField()

    id = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
    post = models.ForeignKey('Post', on_delete=models.CASCADE, null=False)
    user = models.ForeignKey("site.ForumUser", on_delete=models.DO_NOTHING, editable=False)
    date_modified = models.DateTimeField(auto_now=True)
    date_created = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        # Order by date created descending
        ordering = ['-date_created']

    def is_owner(self, user: ForumUser):
        # Checks if a user is the user referenced by the comment
        return user == self.user

    def get_short_date(self) -> str:
        # Get date created, formatted Month day, Hour:Minute:Second
        return self.date_created.strftime("%b %d, %H:%M:%S")

    def get_time_modified(self):
        # Get date modified, formatted Month dat, Hour:Minute:Second
        return self.date_modified.strftime("%b %d, %H:%M:%S")

    def get_absolute_url(self):
        # Return the url of the post referenced by the comment
        return self.post.get_absolute_url()
