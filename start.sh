#!/bin/bash

exec gunicorn Forum.wsgi:application --bind 0.0.0.0:8000 --workers 2
